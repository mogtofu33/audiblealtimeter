# README #

This is an audible altimeter application for android phone with barometer chips (as nexus 4, probably 5 and 6).

Main utility is skydiving, you can use this application to get nice musics and sounds for your alerts when falling from the sky ;)

**DISCLAIMER**: This application IS NOT a replacment for any standard security/information devices used in skydiving, this is just a Proof of Concept not intend to be used in real skydiving.

### TOOLS ###

Build with Android Studio: http://developer.android.com/tools/studio/index.html

### REQUEST AND BUGS ###

I have currently not enough time to enhanced this application, but feel free to use it, enhanced it and submit patches.