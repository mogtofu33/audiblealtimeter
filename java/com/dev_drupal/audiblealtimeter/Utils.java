package com.dev_drupal.audiblealtimeter;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

public class Utils {

    /**
     * Convert list of string to list of integer
     * @param values list of strings to convert
     * @param defaults use instead of values if null (probably not needed)
     * @return list of integer
     */
    public static List<Integer> stringToInt(Set<String> values, List<Integer> defaults) {
        if (values == null) {
            return defaults;
        }
        else {
            List<Integer> converted = new ArrayList<>();
            for (String value : values) {
                converted.add(Integer.parseInt(value));
            }
            Collections.sort(converted);
            return converted;
        }
    }

    /**
     * Roud value.
     * @param value to round
     * @param places set precision
     * @return rounded value!
     */
    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }
}
