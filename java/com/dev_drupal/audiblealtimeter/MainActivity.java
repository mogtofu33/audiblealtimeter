package com.dev_drupal.audiblealtimeter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.media.AudioManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import java.util.List;

public class MainActivity extends Activity implements SharedPreferences.OnSharedPreferenceChangeListener {

    public static final String TAG = MainActivity.class.getName();

    private double speed = 0;
    private int direction = 0;

    public static boolean serviceRestarted = false;

    public TextView txtAltitude;
    public TextView txtPressure;
    public TextView txtSpeed;
    public TextView txtDirection;
    public TextView txtSettings;

    private long logCounter = 0;
    private SharedPreferences sharedPrefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if( Config.DEBUG ) Log.d(TAG, "onCreate");
        setContentView(R.layout.activity_main);

        // Get the SensorManager instance
        SensorManager mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        Sensor mPressure = mSensorManager.getDefaultSensor(Sensor.TYPE_PRESSURE);

        // Check whether the device supports Environmental Sensors
        if (mPressure == null){
            Log.e(TAG, "Device has no pressure sensor");
            //customDialog(R.string.no_pressure_title, R.string.no_pressure, false, false, null);
            finish();
        }
        else {

            // Settings
            sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);

            // Disclaimer
            int disclaimerShown = sharedPrefs.getInt("disclaimer_shown", 0);
            if (Config.PRODUCTION && disclaimerShown == 0) {
                customDialog(R.string.disclaimer_title, R.string.disclaimer, true, true, "disclaimer_shown");
            }

            // Sound
            setVolumeControlStream(AudioManager.STREAM_MUSIC);

            // Start Sensor service
            startService(new Intent(this, SensorService.class));

            // UI
            txtAltitude = (TextView) findViewById(R.id.textViewAltitude);
            txtPressure = (TextView) findViewById(R.id.textViewPressure);
            txtSettings = (TextView) findViewById(R.id.textViewSettings);
            txtSpeed = (TextView) findViewById(R.id.textViewSpeed);
            txtDirection = (TextView) findViewById(R.id.textViewDirection);

            StringBuilder settings = new StringBuilder();
            if (SensorService.alarmsSet) {
                List<Integer> alarmsSelection = Utils.stringToInt(sharedPrefs.getStringSet("alarms", null), Config.DEFAULT_ALARMS);
                settings.append("Alarms: ").append(String.valueOf(alarmsSelection).replace("[", "").replace("]", "")).append("\n");
            } else {
                settings.append("No alarms mode!").append("\n");
            }
            if (SensorService.soundsSet) {
                List<Integer> soundsSelection = Utils.stringToInt(sharedPrefs.getStringSet("sounds", null), Config.DEFAULT_SOUNDS);
                settings.append("Sounds: ").append(String.valueOf(soundsSelection).replace("[", "").replace("]", "")).append("\n");
            } else {
                settings.append("No sounds mode!").append("\n");
            }
            settings.append("DZ altitude: ").append(sharedPrefs.getString("dz_altitude", "0")).append("\n");
            String[] sensorDelayText = getResources().getStringArray(R.array.sensor);
            String sensorDelayValue = sharedPrefs.getString("sensor_delay", "1");
            settings.append("Sensor mode: ").append(sensorDelayText[Integer.parseInt(sensorDelayValue)]);
            txtSettings.setText(settings);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if( Config.DEBUG ) Log.d(TAG, "onPause");
        unregisterReceiver(mBroadcastReceiver);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if( Config.DEBUG ) Log.d(TAG, "onResume");
        registerReceiver(mBroadcastReceiver, new IntentFilter(Config.GET_ALTITUDE));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                Intent SettingsIntent = new Intent(this, SettingsActivity.class);
                startActivity(SettingsIntent);
                sharedPrefs.registerOnSharedPreferenceChangeListener(this);
                break;
            case R.id.action_about:
                Intent AboutIntent = new Intent(this, AboutActivity.class);
                startActivity(AboutIntent);
                break;
            case R.id.action_exit:
                // Stop Sensor service only when explicitly quit
                serviceRestarted = false;
                stopService(new Intent(this, SensorService.class));
                finish();
                break;
            case R.id.action_restart:
                serviceRestarted = true;
                stopService(new Intent(this, SensorService.class));
                startService(new Intent(this, SensorService.class));
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Define receiver
     */
    private BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            MainActivity.this.receivedBroadcast(intent);
        }
    };

    /**
     * Altitude receiver
     * @param intent used to pass value
     */
    private void receivedBroadcast(Intent intent) {
        if(intent.getAction().equalsIgnoreCase(Config.GET_ALTITUDE)) {
            ++logCounter;
            if ((logCounter % Config.UI_UPDATE) == 0L) {
                // Get rounded values
                int pressure = (int) intent.getDoubleExtra("pressure", 0.0);
                int altitude = (int) intent.getDoubleExtra("altitude", 0.0);
                if (sharedPrefs.getBoolean("speed", false)) {
                    speed  = (int) intent.getDoubleExtra("speed", 0.0);
                    direction  = intent.getIntExtra("direction", 0);
                    String uiDirection = "Idle";
                    switch (direction) {
                        case Config.DIRECTION_UP:
                            uiDirection = "Going up";
                            break;
                        case Config.DIRECTION_DOWN:
                            uiDirection = "Going down";
                            break;
                    }
                    StringBuilder speedText = new StringBuilder();
                    speedText.append(String.valueOf(speed)).append(" m/s").append("\n");
                    speedText.append(String.valueOf(speed*3.6)).append(" km/h").append("\n");
                    speedText.append(String.valueOf(speed*1.94)).append(" kt");
                    txtSpeed.setText(speedText);
                    txtDirection.setText(uiDirection);
                }
                txtAltitude.setText(String.valueOf(altitude) + " m");
                txtPressure.setText(String.valueOf(pressure) + " hPa");
            }
            // Reset count
            if (logCounter >= 10000) logCounter = 0;
        }
    }

    /**
     * Create custom dialog box
     * @param title of the dialog
     * @param msg of the dialog
     */
    public void customDialog(int title, int msg, boolean positiveButton, final boolean okSetPreference, final String key) {
        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(this)
                .setMessage(msg)
                .setTitle(title)
                .setCancelable(false);
        if (positiveButton) {
            alertBuilder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    if (okSetPreference) {
                        SharedPreferences.Editor editor = sharedPrefs.edit();
                        editor.putInt(key, 1);
                        editor.apply();
                    }
                    // Nothing happen
                }
            });
        }

        alertBuilder.setNegativeButton(R.string.action_exit, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog, exit
                        serviceRestarted = false;
                        stopService(new Intent(MainActivity.this, SensorService.class));
                        finish();
                    }
                });
        AlertDialog dialog = alertBuilder.create();
        // show dialog
        dialog.show();
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String s) {
        if (Config.DEBUG) Log.d(TAG, "onSharedPreferenceChanged");
        serviceRestarted = true;
        stopService(new Intent(this, SensorService.class));
        startService(new Intent(this, SensorService.class));
    }

}
