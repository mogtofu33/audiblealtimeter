package com.dev_drupal.audiblealtimeter;

import android.content.Context;
import android.content.SharedPreferences;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.ListPreference;
import android.preference.MultiSelectListPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.preference.RingtonePreference;
import android.text.TextUtils;
import android.util.Log;

import java.util.List;

public class SettingsFragment extends PreferenceFragment implements SharedPreferences.OnSharedPreferenceChangeListener {

    public static final String TAG = SettingsFragment.class.getName();

    public SharedPreferences sharedPreferences;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Context mContext = ApplicationContextProvider.getContext();

        // Load the preferences from an XML resource
        addPreferencesFromResource(R.xml.preferences);
        PreferenceManager.setDefaultValues(mContext, R.xml.preferences, false);

        // Set summary
        sharedPreferences = getPreferenceScreen().getSharedPreferences();
        String[] attached = {"sounds", "alarms", "dz_altitude", "threshold_pressure",
                "sensor_delay", "simulate_start", "under_canopy", "sensibility_fall_min",
                "sensibility_fall_max", "sensibility_canopy_min", "sensibility_canopy_max"};
        for (String attach : attached) {
            bindPreferenceSummaryToValue(attach);
        }
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (Config.DEBUG) Log.d(TAG, "onSharedPreferenceChanged: " + key);
        bindPreferenceSummaryToValue(key);
    }

    @Override
    public void onResume() {
        super.onResume();
        getPreferenceManager().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        getPreferenceManager().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
    }

    /**
     * Binds a preference's summary to its value. More specifically, when the
     * preference's value is changed, its summary (line of text below the
     * preference title) is updated to reflect the value. The summary is also
     * immediately updated upon calling this method. The exact display format is
     * dependent on the type of preference.
     */
    private void bindPreferenceSummaryToValue(String key) {
        Preference preference = findPreference(key);
        // Not needed
        if (preference instanceof CheckBoxPreference) {
            return;
        }

        if (preference instanceof MultiSelectListPreference) {
            List<Integer> stringValue = Utils.stringToInt(((MultiSelectListPreference) preference).getValues(), null);
            preference.setSummary(String.valueOf(stringValue).replace("[", "").replace("]", ""));

        } else if (preference instanceof ListPreference) {
            String stringValue = sharedPreferences.getString(key, "0");
            // For list preferences, look up the correct display value in
            // the preference's 'entries' list.
            ListPreference listPreference = (ListPreference) preference;
            int index = listPreference.findIndexOfValue(stringValue);

            // Set the summary to reflect the new value.
            preference.setSummary(
                    index >= 0
                            ? preference.getSummary() + " " + listPreference.getEntries()[index]
                            : null);
        } else if (preference instanceof RingtonePreference) {
            String stringValue = sharedPreferences.getString(key, "0");
            // For ringtone preferences, look up the correct display value
            // using RingtoneManager.
            if (TextUtils.isEmpty(stringValue)) {
                // Empty values correspond to 'silent' (no ringtone).
                preference.setSummary(R.string.pref_ringtone_silent);

            } else {
                Ringtone ringtone = RingtoneManager.getRingtone(
                        preference.getContext(), Uri.parse(stringValue));

                if (ringtone == null) {
                    // Clear the summary if there was a lookup error.
                    preference.setSummary(null);
                } else {
                    // Set the summary to reflect the new ringtone display
                    // name.
                    String name = ringtone.getTitle(preference.getContext());
                    preference.setSummary(name);
                }
            }
        } else {
            String stringValue = sharedPreferences.getString(key, "0");
            preference.setSummary(stringValue);
        }
    }
}
