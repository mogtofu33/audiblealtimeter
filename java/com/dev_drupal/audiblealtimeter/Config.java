package com.dev_drupal.audiblealtimeter;

import java.util.Arrays;
import java.util.List;

public class Config {

    public static final boolean DEBUG = false;
    public static final boolean VERBOSE = false;
    public static final boolean PRODUCTION = false;

    public static final int     NOTIFICATION_ID = 1;
    public static final long    UI_UPDATE = 10L;
    public static final int     SCREEN_OFF_RECEIVER_DELAY = 500;
    // Only if verbose
    public static final long    MINIMAL_ENERGY_PERIOD = 100L;

    // Simulate settings
    public static final long    SIMULATE_PERIOD = 10L;
    public static final int     LOW_INCREMENT = 1;
    public static final int     FAST_INCREMENT = 10;

    // Speed calculator
    public static final int     SAMPLING = 1000;
    // Direction calculator
    public static final int     DIRECTION_UP = 1;
    public static final int     DIRECTION_DOWN = -1;
    public static final int     DIRECTION_IDLE = 0;

    public static final List<Integer> DEFAULT_ALARMS = Arrays.asList(1500, 1000);
    public static final List<Integer> DEFAULT_SOUNDS = Arrays.asList(10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 200, 300, 400, 500, 600, 700, 800, 2000, 2500, 3000, 3500, 4000);

    public static final String  GET_ALTITUDE = "com.dev_drupal.audiblealtimeter.intent.action.GET_ALTITUDE";

    /**
     * Suppress default constructor for non instability
     */
    private Config() {
        throw new AssertionError();
    }
}
