package com.dev_drupal.audiblealtimeter;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Build;
import android.util.Log;
import java.util.List;

public class AltitudeReceiver extends BroadcastReceiver {

    private final String TAG = AltitudeReceiver.class.getName();

    private int prevAltitudeSound = 0;
    private boolean isPlaying = false;
    private static MediaPlayer mPlayer;

    @Override
    public void onReceive(Context context, Intent intent) {
        if(intent.getAction().equalsIgnoreCase(Config.GET_ALTITUDE)) {
            // Get rounded altitude, pressure not needed
            int altitude = (int) intent.getDoubleExtra("altitude", 0.0);

            // Check alarm and sounds
            if (!isPlaying) {
                if (SensorService.alarmsSet) {
                    check(context, altitude, SensorService.alarms, Config.DEFAULT_ALARMS, R.raw.alarm);
                }
                if (SensorService.soundsSet) {
                    check(context, altitude, SensorService.sounds, Config.DEFAULT_SOUNDS, 0);
                }
            }
        }
    }

    /**
     * Check if an altitude is in targets and play sound
     * @param context application context
     * @param altitude altitude to check rounded to int
     * @param targets list of altitudes that trigger sound
     * @param defaults list of defaults altitudes if targets null
     * @param sound resources id sound to play or altitude sounds if 0
     */
    public void check(Context context, int altitude, List<Integer> targets, List<Integer> defaults, int sound) {
        int altitudeSound = checkAlarms(altitude, targets, defaults);
        // Check if sound and not the previous one
        if (altitudeSound > 0 && altitudeSound != prevAltitudeSound) {
            if (sound != 0) {
                if (Config.VERBOSE) Log.i(TAG, "Alarm: " + altitudeSound);
                playSound(sound, context);
            }
            else {
                if (Config.VERBOSE) Log.i(TAG, "Sound: " + altitudeSound);
                playSoundsAltitude(altitudeSound, context);
            }
            prevAltitudeSound = altitudeSound;
        }
    }

    /**
     * Check if target altitude is found in current altitude.
     * @param altitude current altitude to check
     * @param targets list of targets to check
     * @param defaults default list if targets empty
     * @return altitude that trigger alarms or -1
     */
    private int checkAlarms(int altitude, List<Integer> targets, List<Integer> defaults) {
        if (targets == null) {
            targets = defaults;
        }
        for (int target : targets) {
            if (checkIfAlarm(altitude, target)) {
                return target;
            }
        }
        return -1;
    }

    /**
     * Check if altitude is in target depending sensibility.
     * @return boolean
     */
    public boolean checkIfAlarm(int altitude, int target) {
        // check alarm
        if (target != 0) {
            // Handle dz altitude
            altitude = altitude + SensorService.dz_altitude;
            // Detect situation
            int sensibility_min, sensibility_max;
            if (altitude > SensorService.underCanopy) {
                sensibility_min = SensorService.sensFallMin;
                sensibility_max = SensorService.sensFallMax;
            } else {
                sensibility_min = SensorService.sensCanopyMin;
                sensibility_max = SensorService.sensCanopyMax;
            }
            // Check altitude
            if (altitude > (target - sensibility_min) && altitude < (target + sensibility_max)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Play sound resource.
     * @param sound resource id.
     * @param context application context.
     */
    public void playSound(int sound, Context context) {
        // Request audio focus for playback
        int requestMode = AudioManager.AUDIOFOCUS_GAIN_TRANSIENT;
        if (Build.VERSION.SDK_INT > 19) {
            requestMode = AudioManager.AUDIOFOCUS_GAIN_TRANSIENT_EXCLUSIVE;
        }
        int result = SensorService.mAudioManager.requestAudioFocus(mAudioFocusListener,
                AudioManager.STREAM_MUSIC,
                // Temporary request with no other system playing disruptive sounds.
                requestMode);
        // If request ok
        if (result == AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
            if (Config.VERBOSE) Log.v(TAG, "Audio focus request granted");
            isPlaying = true;
            mPlayer = MediaPlayer.create(context, sound);
            // Avoid "Should have subtitle controller already set"
            mPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            // Add listener for completion
            mPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    // Abandon audio focus when playback complete
                    SensorService.mAudioManager.abandonAudioFocus(mAudioFocusListener);
                    // Avoid "media player went away with unhandled events"
                    mPlayer.reset();
                    // Release mPlayer
                    mPlayer.release();
                    mPlayer = null;
                    isPlaying = false;
                }

            });
            mPlayer.start();
        }
        else {
            Log.e(TAG, "No audio focus");
        }
    }

    /**
     * Get sound for sounds !
     * @param altitude to find sound for.
     * @param context application context.
     */
    public void playSoundsAltitude(int altitude, Context context) {
        switch (altitude) {
            case 4500:
                playSound(R.raw.altitude_4500, context);
                break;
            case 4250:
                playSound(R.raw.altitude_4250, context);
                break;
            case 4000:
                playSound(R.raw.altitude_4000, context);
                break;
            case 3750:
                playSound(R.raw.altitude_3750, context);
                break;
            case 3500:
                playSound(R.raw.altitude_3500, context);
                break;
            case 3250:
                playSound(R.raw.altitude_3250, context);
                break;
            case 3000:
                playSound(R.raw.altitude_3000, context);
                break;
            case 2750:
                playSound(R.raw.altitude_2750, context);
                break;
            case 2500:
                playSound(R.raw.altitude_2500, context);
                break;
            case 2250:
                playSound(R.raw.altitude_2250, context);
                break;
            case 2000:
                playSound(R.raw.altitude_2000, context);
                break;
            case 1750:
                playSound(R.raw.altitude_1750, context);
                break;
            case 1500:
                playSound(R.raw.altitude_1500, context);
                break;
            case 1250:
                playSound(R.raw.altitude_1250, context);
                break;
            case 1000:
                playSound(R.raw.altitude_1000, context);
                break;
            case 950:
                playSound(R.raw.altitude_950, context);
                break;
            case 900:
                playSound(R.raw.altitude_900, context);
                break;
            case 850:
                playSound(R.raw.altitude_850, context);
                break;
            case 800:
                playSound(R.raw.altitude_800, context);
                break;
            case 750:
                playSound(R.raw.altitude_750, context);
                break;
            case 700:
                playSound(R.raw.altitude_700, context);
                break;
            case 650:
                playSound(R.raw.altitude_650, context);
                break;
            case 600:
                playSound(R.raw.altitude_600, context);
                break;
            case 550:
                playSound(R.raw.altitude_550, context);
                break;
            case 500:
                playSound(R.raw.altitude_500, context);
                break;
            case 475:
                playSound(R.raw.altitude_475, context);
                break;
            case 450:
                playSound(R.raw.altitude_450, context);
                break;
            case 425:
                playSound(R.raw.altitude_425, context);
                break;
            case 400:
                playSound(R.raw.altitude_400, context);
                break;
            case 375:
                playSound(R.raw.altitude_375, context);
                break;
            case 350:
                playSound(R.raw.altitude_350, context);
                break;
            case 325:
                playSound(R.raw.altitude_325, context);
                break;
            case 300:
                playSound(R.raw.altitude_300, context);
                break;
            case 275:
                playSound(R.raw.altitude_275, context);
                break;
            case 250:
                playSound(R.raw.altitude_250, context);
                break;
            case 225:
                playSound(R.raw.altitude_225, context);
                break;
            case 200:
                playSound(R.raw.altitude_200, context);
                break;
            case 175:
                playSound(R.raw.altitude_175, context);
                break;
            case 150:
                playSound(R.raw.altitude_150, context);
                break;
            case 125:
                playSound(R.raw.altitude_125, context);
                break;
            case 100:
                playSound(R.raw.altitude_100, context);
                break;
            case 90:
                playSound(R.raw.altitude_90, context);
                break;
            case 80:
                playSound(R.raw.altitude_80, context);
                break;
            case 70:
                playSound(R.raw.altitude_70, context);
                break;
            case 60:
                playSound(R.raw.altitude_60, context);
                break;
            case 50:
                playSound(R.raw.altitude_50, context);
                break;
            case 40:
                playSound(R.raw.altitude_40, context);
                break;
            case 30:
                playSound(R.raw.altitude_30, context);
                break;
            case 20:
                playSound(R.raw.altitude_20, context);
                break;
            case 10:
                playSound(R.raw.altitude_10, context);
                break;
        }
    }

    /**
     * Audio focus management.
     * Listener never really trigger onAudioFocusChange...
     */
    private AudioManager.OnAudioFocusChangeListener mAudioFocusListener = new AudioManager.OnAudioFocusChangeListener() {

        @Override
        public void onAudioFocusChange(int focusChange) {
            /*
            if (focusChange == AudioManager.AUDIOFOCUS_LOSS_TRANSIENT) {
                // Pause playback
            } else if (focusChange == AudioManager.AUDIOFOCUS_GAIN) {
                // Resume playback
            } else if (focusChange == AudioManager.AUDIOFOCUS_LOSS) {
                SensorService.mAudioManager.abandonAudioFocus(mAudioFocusListener);
                // Stop playback
            }
            */
        }

    };
}
