package com.dev_drupal.audiblealtimeter;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.app.TaskStackBuilder;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.AudioManager;
import android.os.Handler;
import android.os.IBinder;
import android.os.PowerManager;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class SensorService extends Service implements SensorEventListener {

    private final String TAG = SensorService.class.getName();
    
    private SensorManager mSensorManager = null;
    private PowerManager.WakeLock mWakeLock = null;
    private AltitudeReceiver mAltitudeReceiver;
    private Context mContext;
    private NotificationManager mNotificationManager;

    private long logCounter = 0;
    private boolean simulate = false;
    // Simulate mode, true = up, false = fall
    private boolean simulateMode = true;

    private double altitude = 0;
    private int firstAltitude;
    private double pressure = 0;

    // speed
    private boolean getSpeed = false;
    private double last_altitude = 0;
    private double last_pressure = 0;
    private long elapsedTime = 0L;
    private long previousTime = 0L;
    private double pressureThreshold = 0.2;
    private static double current_speed = 0;
    private static double current_pressure_speed = 0;
    private static int current_direction = Config.DIRECTION_IDLE;

    // sounds and alarms
    public static boolean alarmsSet = true;
    public static boolean soundsSet = true;
    public static int dz_altitude = 0;
    public static List<Integer> alarms = Arrays.asList();
    public static List<Integer> sounds = Arrays.asList();
    public static int underCanopy;
    public static int sensCanopyMin;
    public static int sensCanopyMax;
    public static int sensFallMin;
    public static int sensFallMax;

    public boolean firstReceive = true;
    public boolean startAltitudeReset = false;
    public int sensorDelay = 1;

    public static AudioManager mAudioManager;
    public SharedPreferences sharedPrefs;

    @Override
    public void onCreate() {
        super.onCreate();

        if( Config.DEBUG ) Log.d(TAG, "onCreate");

        mContext = ApplicationContextProvider.getContext();

        mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        mAltitudeReceiver = new AltitudeReceiver();

        PowerManager manager = (PowerManager) getSystemService(Context.POWER_SERVICE);
        mWakeLock = manager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, TAG);

        // Screen off workaround
        // http://nosemaj.org/android-persistent-sensors
        // http://www.saltwebsites.com/2012/android-accelerometers-screen-off
        registerReceiver(mWakeLockReceiver, new IntentFilter(Intent.ACTION_SCREEN_OFF));

        // Sounds receiver
        registerReceiver(mAltitudeReceiver, new IntentFilter(Config.GET_ALTITUDE));
        // Not on onStart to avoid re-launch when coming back from preferences
        registerListener();

        // Prepare audio
        mAudioManager = (AudioManager) mContext.getSystemService(Context.AUDIO_SERVICE);

        // Get settings
        sharedPrefs =  PreferenceManager.getDefaultSharedPreferences(mContext);
        // Used on AltitudeReceiver
        alarmsSet = sharedPrefs.getBoolean("alarms_altitude", true);
        soundsSet = sharedPrefs.getBoolean("sounds_altitude", true);
        alarms = Utils.stringToInt(sharedPrefs.getStringSet("alarms", null), Config.DEFAULT_ALARMS);
        sounds = Utils.stringToInt(sharedPrefs.getStringSet("sounds", null), Config.DEFAULT_SOUNDS);
        underCanopy = Integer.parseInt(sharedPrefs.getString("under_canopy", "900"));
        sensCanopyMin = Integer.parseInt(sharedPrefs.getString("sensibility_canopy_min", "0"));
        sensCanopyMax = Integer.parseInt(sharedPrefs.getString("sensibility_canopy_max", "5"));
        sensFallMin = Integer.parseInt(sharedPrefs.getString("sensibility_fall_min", "0"));
        sensFallMax = Integer.parseInt(sharedPrefs.getString("sensibility_fall_max", "100"));
        dz_altitude = Integer.parseInt(sharedPrefs.getString("dz_altitude", "0"));
        // Local use
        firstAltitude = sharedPrefs.getInt("first_altitude", 0);
        startAltitudeReset = sharedPrefs.getBoolean("start_altitude_reset", true);
        sensorDelay = Integer.parseInt(sharedPrefs.getString("sensor_delay", "1"));
        simulate = sharedPrefs.getBoolean("simulate", false);
        getSpeed = sharedPrefs.getBoolean("speed", false);
        pressureThreshold = Double.parseDouble(sharedPrefs.getString("threshold_pressure", "0.2"));
        if (simulate) {
            altitude = (double) Integer.parseInt(sharedPrefs.getString("simulate_start", "0"));
        }

        if (MainActivity.serviceRestarted) {
            Toast.makeText(getApplicationContext(), "Service restarted", Toast.LENGTH_SHORT).show();
        }
        else {
            Toast.makeText(getApplicationContext(), "Service started", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);

        if( Config.DEBUG ) Log.d(TAG, "onStartCommand " + startId);

        createNotification(Config.NOTIFICATION_ID, R.drawable.ic_action_time, "Audible Altimeter", "Audible altimeter running, click here to view.");
        mWakeLock.acquire();

        return START_STICKY;
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {

        // Get value
        if (simulate) {
            firstAltitude = 0;
            pressure = SensorManager.PRESSURE_STANDARD_ATMOSPHERE;
            altitude = simulate(altitude);
        }
        else {
            // Get pressure
            pressure = sensorEvent.values[0];
            // Get altitude
            altitude = SensorManager.getAltitude(SensorManager.PRESSURE_STANDARD_ATMOSPHERE, (float)pressure);
            // Debug loop to log
            if (Config.VERBOSE) {
                ++logCounter;
                if ((logCounter % Config.MINIMAL_ENERGY_PERIOD) == 0L) {
                    Log.v(TAG, "Sensor: " + logCounter + " at " + new Date().toString() + ": " + String.valueOf(altitude));
                }
            }
        }

        // Get speed
        if (getSpeed) {
            long currentTime = System.currentTimeMillis();
            elapsedTime += currentTime - previousTime;

            if (elapsedTime > Config.SAMPLING) {
                // Detect if we are going up or down
                double delta_pressure = (pressure - last_pressure);
                double delta_altitude = (altitude - last_altitude);

                last_pressure = pressure;
                last_altitude = altitude;

                current_speed = Utils.round(delta_altitude / (elapsedTime / 1000), 3); // in meters/second
                current_pressure_speed = Utils.round(delta_pressure / (elapsedTime / 1000), 3); // in mPa/second

                if ( Config.VERBOSE) Log.v(TAG, "current_pressure_speed: " + current_pressure_speed);

                if (Math.abs(current_pressure_speed) <= pressureThreshold) {
                    current_direction = Config.DIRECTION_IDLE;
                }
                // For pressure, positive values is down
                else if (current_pressure_speed > pressureThreshold) {
                    current_direction = Config.DIRECTION_DOWN;
                }
                // Negative is up
                else {
                    current_direction = Config.DIRECTION_UP;
                }

                // reset
                elapsedTime = 0;
            }
            previousTime = currentTime;
        }

        // Reset start altitude if needed
        if (!simulate && firstReceive && startAltitudeReset && !MainActivity.serviceRestarted) {
            firstAltitude = (int)altitude;
            // Store first altitude
            setIntPreference("first_altitude", firstAltitude);
            if( Config.DEBUG ) Log.d(TAG, "first_altitude SET TO: " + firstAltitude);
            firstReceive = false;
        }
        altitude = altitude - firstAltitude;

        // Send value
        Intent intent = new Intent();
        intent.setAction(Config.GET_ALTITUDE);
        intent.putExtra("pressure", pressure);
        intent.putExtra("altitude", altitude);
        // speed
        if (getSpeed) {
            intent.putExtra("speed", current_speed);
            intent.putExtra("direction", current_direction);
        }
        // Send to broadcast new altitude and pressure
        mContext.sendBroadcast(intent);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {
        if( Config.DEBUG ) Log.d(TAG, "onAccuracyChanged " + String.valueOf(sensor) + ": " + String.valueOf(i));
    }

    @Override
    public void onDestroy() {
        if( Config.DEBUG ) Log.d(TAG, "onDestroy");

        if (!MainActivity.serviceRestarted) {
            if( Config.DEBUG ) Log.d(TAG, "Quit, not restarted, reset first altitude");
            // Reset first altitude
            setIntPreference("first_altitude", 0);
        }

        unregisterReceiver(mWakeLockReceiver);
        unregisterReceiver(mAltitudeReceiver);
        unregisterListener();
        mWakeLock.release();
        mNotificationManager.cancel(Config.NOTIFICATION_ID);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    /**
     * Create notification
     * http://guides.codepath.com/android/Notifications
     * http://developer.android.com/guide/topics/ui/notifiers/notifications.html
     * @param nId unique notification id
     * @param iconRes resource icon
     * @param title notification title
     * @param body notification body
     */
    private void createNotification(int nId, int iconRes, String title, String body) {
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(iconRes)
                        .setContentTitle(title)
                        .setContentText(body)
                        .setOngoing(true);
        Intent resultIntent = new Intent(this, MainActivity.class);

        // The stack builder object will contain an artificial back stack for the
        // started Activity.
        // This ensures that navigating backward from the Activity leads out of
        // your application to the Home screen.
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        // Adds the back stack for the Intent (but not the Intent itself)
        stackBuilder.addParentStack(MainActivity.class);
        // Adds the Intent that starts the Activity to the top of the stack
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(
                        0,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );
        mBuilder.setContentIntent(resultPendingIntent);
        mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        // nId allows you to update the notification later on.
        mNotificationManager.notify(nId, mBuilder.build());
    }

    /**
     * Screen off workaround
     * http://nosemaj.org/android-persistent-sensors
     * http://www.saltwebsites.com/2012/android-accelerometers-screen-off
     */
    public BroadcastReceiver mWakeLockReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.i(TAG, "onReceive("+intent+")");

            if (!intent.getAction().equals(Intent.ACTION_SCREEN_OFF)) {
                return;
            }

            Runnable runnable = new Runnable() {
                public void run() {
                    Log.i(TAG, "Runnable executing.");
                    unregisterListener();
                    registerListener();
                }
            };

            new Handler().postDelayed(runnable, Config.SCREEN_OFF_RECEIVER_DELAY);
        }
    };

    /*
     * Register this as a sensor event listener.
     */
    private void registerListener() {
        if( Config.DEBUG ) Log.d(TAG, "registerListener");
        Sensor mPressure = mSensorManager.getDefaultSensor(Sensor.TYPE_PRESSURE);
        mSensorManager.registerListener(this, mPressure, sensorDelay);
    }

    /*
     * Un-register this as a sensor event listener.
     */
    private void unregisterListener() {
        if( Config.DEBUG ) Log.d(TAG, "unregisterListener");
        mSensorManager.unregisterListener(this);
    }

    /**
     * Simulate altitude up and down
     * @param altitude current altitude
     * @return altitude
     */
    private double simulate(double altitude) {
        int roundedAltitude = (int) altitude;
        ++logCounter;
        if ((logCounter % Config.SIMULATE_PERIOD) == 0L) {
            if (roundedAltitude == 4000) {
                simulateMode = false;
            }
            else if (roundedAltitude <= 0) {
                simulateMode = true;
            }
            // up (in plane)
            if (roundedAltitude < 4000 && simulateMode) {
                roundedAltitude = (roundedAltitude + Config.LOW_INCREMENT);
            }
            // free fall and canopy
            if (roundedAltitude > 0 && !simulateMode) {
                // under canopy
                if (roundedAltitude < SensorService.underCanopy) {
                    roundedAltitude = (roundedAltitude - Config.LOW_INCREMENT);
                }
                // free fall
                else {
                    roundedAltitude = (roundedAltitude - Config.FAST_INCREMENT);
                }
            }
            altitude = (double) roundedAltitude;
            if (Config.DEBUG)
                Log.d(TAG, "Simulate: " + logCounter + " at " + new Date().toString() + ": " + altitude);
        }
        return altitude;
    }

    /**
     * Save preference.
     * @param value to set
     */
    public void setIntPreference(String key, int value) {
        SharedPreferences.Editor editor = sharedPrefs.edit();
        editor.putInt(key, value);
        editor.apply();
    }
}
